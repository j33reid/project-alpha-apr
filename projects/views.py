from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.forms import ProjectForm


@login_required
def projects_list(request):
    project_brief = Project.objects.filter(owner=request.user)
    context = {"project_brief_object": project_brief}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project_detail = Project.objects.get(id=id)
    task_detail = Task.objects.all()
    context = {
        "project_detail": project_detail,
        "task_detail": task_detail,
    }
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }

    return render(request, "projects/create.html", context)
